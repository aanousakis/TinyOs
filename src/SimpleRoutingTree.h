#ifndef SIMPLEROUTINGTREE_H
#define SIMPLEROUTINGTREE_H


enum{							// AM : active message ID
	SENDER_QUEUE_SIZE=5,
	RECEIVER_QUEUE_SIZE=3,
	AM_SIMPLEROUTINGTREEMSG=22, //id typos mhnymatos
	AM_ROUTINGMSG=22,			//id typos mhnymatos
	AM_NOTIFYPARENTMSG=12,		//id typos mhnymatos
	SEND_CHECK_MILLIS=70000,
	TIMER_PERIOD_MILLI=60000,	//mia epoxi!!!
	TIMER_FAST_PERIOD=200,
	TIMER_LEDS_MILLI=1000,
	Rounds=10,
//	Max_Children=40,
//	Max_depth=TIMER_PERIOD_MILLI/(Max_Children*broadcastTime),  // poso einai to megisto bathos
//	treeInit=Max_depth*broadcastTime,                           // posi ora diarkei i arxikopoiisi tou dendrou se ms
//	timePerDepth=TIMER_PERIOD_MILLI/Max_depth,                   // posi ora diarkei i apostoli ton munimaton ana epipedo se ms

	Max_depth=50,
	window=TIMER_PERIOD_MILLI/Max_depth,
	timePerDepth=200,    
	//timePerDepth=window,					                                         // posi ora diarkei i apostoli ton munimaton ana epipedo se ms
	broadcastTime=20,                                            // posi ora thelei gia na stalei ena munima se ms
	treeInit=Max_depth*broadcastTime                             // posi ora diarkei i arxikopoiisi tou dendrou se ms
};  
/*uint16_t AM_ROUTINGMSG=AM_SIMPLEROUTINGTREEMSG;
uint16_t AM_NOTIFYPARENTMSG=AM_SIMPLEROUTINGTREEMSG;
*/

//Dhlwsh duo typwn mhnymatwn
typedef nx_struct RoutingMsg	//me ayto tha stelnw to erwthma
{
	/*otan stelnw pragmata sto diktuo kalo einai ta pedia
	 * aytwn twn pragmatwn na 3ekinnoun me to nx gia na
	 * douleuoun swsta ane3arthtou arxitektonikhs*/
	nx_uint16_t senderID;	//id aytoy poy esteile to mnm 16bit
	nx_uint8_t  depth;		//ba8os aytoy poy esteile to mnm 8bit
	//nx_uint16_t  responce;   // o pateras stelnei sto paidi toy pote perimenei na toy apantisei, epoxi/bathos
} RoutingMsg;

typedef nx_struct NotifyParentMsg	//eidopoiw patera oti eimai paidi toy
{
	nx_uint16_t senderID;	//poios eimai pou to stelnw
	nx_uint16_t parentID;	//autos einai o pateras moy
	nx_uint8_t  depth;		//ayto einai to ba8os moy
	nx_uint16_t count;
	nx_uint16_t sum;
	nx_uint32_t sum2;
} NotifyParentMsg;


#endif
