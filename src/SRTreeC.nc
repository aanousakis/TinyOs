#include "SimpleRoutingTree.h"


module SRTreeC
{
	//ola ta interfaces pou 8a xrhsimopoihsw
	uses interface Boot;
	//xeirismos radiofwnoy
	uses interface SplitControl as RadioControl;


	//gia ka8e typo mhnymatos mia triada
	uses interface AMSend as RoutingAMSend;
	uses interface AMPacket as RoutingAMPacket;
	uses interface Packet as RoutingPacket;
	
	uses interface AMSend   as NotifyAMSend;
	uses interface AMPacket as NotifyAMPacket;
	uses interface Packet   as NotifyPacket;

	uses interface Timer<TMilli> as RoutingMsgTimer;
	uses interface Timer<TMilli> as ResponceTimer;     //<------------------------------------
	
	uses interface Receive as RoutingReceive;
	uses interface Receive as NotifyReceive;
	
	//gia ka8e typo mhnymatos 2 oyres-mia lambanw mia stelnw
	uses interface PacketQueue as RoutingSendQueue;
	uses interface PacketQueue as RoutingReceiveQueue;
	//einai mia oyra mhnymatwn poy exei do8eo den exei
	//shmasia na katalaboyme ti kanei
	uses interface PacketQueue as NotifySendQueue;
	uses interface PacketQueue as NotifyReceiveQueue;
}
implementation
{
	//ayton ton idio kwdika trexoyn oloi oi ais8hthres
	//toys 3exwrizoyme apo to ID toys
	
	//metraei se poia epoxh briskomai
	//ka8e fora pou xtypaei kati enas apo toys metrhthes mou
	//ton ay3anw kata ena
	uint16_t  roundCounter;
	
	//metablhtes typoy message_t
	//oti mhnyma stelnw h lambanw einai typoy message_t
	message_t radioRoutingSendPkt;
	message_t radioNotifySendPkt;
	
	
	bool RoutingSendBusy=FALSE;
	bool NotifySendBusy=FALSE;


	//den xreiazetai
	bool lostRoutingSendTask=FALSE;
	bool lostNotifySendTask=FALSE;
	bool lostRoutingRecTask=FALSE;
	bool lostNotifyRecTask=FALSE;
	
	
	//ba8os komboy
	uint8_t curdepth;
	//poios einai o pateras moy
	uint16_t parentID;
	
	uint16_t sum_tmp;          // Sum(X)
	uint32_t sum2_tmp;         // Sum(X^2)
	uint16_t count_tmp;
	
	//dhlwsh poia tasks 8a xrhsimopoihsw
	task void sendRoutingTask();
	task void sendNotifyTask();
	task void receiveRoutingTask();
	task void receiveNotifyTask();
	
	void setLostRoutingSendTask(bool state)
	{
		atomic{
			lostRoutingSendTask=state;
		}
		if(state==TRUE)
		{
			//call Leds.led2On();
		}
		else 
		{
			//call Leds.led2Off();
		}
	}
	
	void setLostNotifySendTask(bool state)
	{
		atomic{
		lostNotifySendTask=state;
		}
		
		if(state==TRUE)
		{
			//call Leds.led2On();
		}
		else 
		{
			//call Leds.led2Off();
		}
	}
	
	void setLostNotifyRecTask(bool state)
	{
		atomic{
		lostNotifyRecTask=state;
		}
	}
	
	void setLostRoutingRecTask(bool state)
	{
		atomic{
		lostRoutingRecTask=state;
		}
	}
	void setRoutingSendBusy(bool state)
	{
		atomic{
		RoutingSendBusy=state;
		}
		
		dbg("SRTreeC","In function setRoutingSendBusy(...) : RoutingSendBusy = %s\n", (state == TRUE)?"TRUE":"FALSE");
		if(state==TRUE)
		{
			
		}
		else 
		{
			//call Leds.led0Off();
		}
	}
	
	void setNotifySendBusy(bool state)
	{
		atomic{
		NotifySendBusy=state;
		}
		dbg("SRTreeC","In function setNotifySendBusy(...)  : NotifySendBusy  = %s\n", (state == TRUE)?"TRUE":"FALSE");

		if(state==TRUE)
		{
			
		}
		else 
		{
			//call Leds.led1Off();
		}
	}
	
	//edw 3ekina o ais8hthras
	event void Boot.booted()
	{
		/////// arxikopoiisi radio kai serial
		call RadioControl.start();
		//edw akoma den exei 3ekinhsei sigoyra to radio 
		//prepei na er8ei signal oti 3ekinhse
		
		//ayto den xreizotan giati exei dhlw8ei apo panw
		setRoutingSendBusy(FALSE);
		setNotifySendBusy(FALSE);

		//eimaste ston gyro-epoxh 0
		roundCounter =0;
		
		//an eimai o ais8hthras me ID=0 dhladh h riza toy dentroy
		if(TOS_NODE_ID==0)
		{
			curdepth=0;
			parentID=0;
			dbg("Boot", "In event    Boot.booted()           : curdepth = %d  ,  parentID= %d \n", curdepth , parentID);
		}
		//an den eimai o kombos 0 prepei na dhlwsw oti den exw
		//brei akoma poios einai o pateras moy kai poio einai 
		//to ba8os moy
		else
		{
			curdepth=-1;
			parentID=-1;
			dbg("Boot", "In event    Boot.booted()           : curdepth = %d  ,  parentID= %d \n", curdepth , parentID);
		}
	}
	//signal oti 3ekinhse to radiofwno
	event void RadioControl.startDone(error_t err)
	{
		//epeidh kanoyme simulation panta success 8a einai
		if (err == SUCCESS)
		{
			//mporw na pros8esw TOS_NODE_ID gia na mou leei
			//poianoy ais8hthra to radio 3ekinhse
			dbg("Radio" , "Radio initialized successfully!!!\n");

			if (TOS_NODE_ID==0)
			{
				//3ekinsa ton timer RoutingMsgTimer poy 8a
				//xtyphsei mia fora wste na prolaboyn na 
				//3ypnhsoyn oi komboi na 3ekihsei to radiofwno toys
				//giati yparxei keno anamesa sto ka8e boot
				//an kapoia stigmh to tre3w kai den exoyn prolabei na 
				//anoi3oyn oloi ay3anw thn parametro edw
				call RoutingMsgTimer.startOneShot(TIMER_FAST_PERIOD);
				//meta apo edw psaxnw gia fired
			}
		}
		else
		{
			//de 8a mpei edw giati kanoyme simulation kai panta
			//8a 3ekinaei swsta
			dbg("Radio" , "Radio initialization failed! Retrying...\n");
			call RadioControl.start();
		}
	}
	
	event void RadioControl.stopDone(error_t err)
	{ 
		dbg("Radio", "Radio stopped!\n");
	}
	
	//o metrhths xtypaei
	event void RoutingMsgTimer.fired()
	{
		//to PAKETO mhnymatos poy 8elw na steilw afoy to kanw
		//prwta cast se deikth mhnymatwn
		message_t tmp;
		
		error_t enqueueDone;
		
		//deikths se ena RoutingMsg, poy to RoutingMsg einai
		//ena apo ta nx_struct poy dhlwsa sto .h arxeio
		//deikths gia na synde8ei se dedomena mhnymatos
		RoutingMsg* mrpkt;
		dbg("SRTreeC", "In event RoutingMsgTimer.fired(...) : RoutingMsgTimer fired!  radioBusy = %s \n",(RoutingSendBusy)?"True":"False");

		if (TOS_NODE_ID==0)
		{
			//roundCounter+=1;
			
			dbg("SRTreeC", "##################################### \n");
			dbg("SRTreeC", "#######   ROUND   %u    ############## \n", roundCounter+1);
			dbg("SRTreeC", "#####################################\n");
			
			//bazw to metrhth na 3anaxtyphsei meta apo ligo 
			//gia na kanei thn idia diadikasia
			//call RoutingMsgTimer.startOneShot(TIMER_PERIOD_MILLI);  //<-------------------------------------
		}
		
		//elegxei an h oyra poy exei sxesh me routing mhnymata
		//einai gemath - de 8a einai ...
		if(call RoutingSendQueue.full())
		{
			dbg("SRTreeC", "In event RoutingMsgTimer.fired(...) : Error RoutingSendQueue is FULL\n");
			return;
		}
		
		
		//dhmioyrgw mhnyma kai to bazw sthn oyra
		//getPayLoad epistrefei deikth sta dedomena toy mhnymatos
		//kai to kanoyme cast ston deikth mrpkt
		mrpkt = (RoutingMsg*) (call RoutingPacket.getPayload(&tmp, sizeof(RoutingMsg)));
		//den prokeitai na epistrepsei pote null
		if(mrpkt==NULL)
		{
			dbg("SRTreeC","In event RoutingMsgTimer.fired(...) : Error No valid payload... \n");
			return;
		}
		//to atomic den mporei na diakopei apo kapoio pi8ano signal
		atomic{
			//poios esteile to mhnyma
			mrpkt->senderID=TOS_NODE_ID;
			//se ti ba8os einai aytos poy esteile to mhnyma
			mrpkt->depth = curdepth;
		}

		//den exw sygkekrimeno apodekth giayto bazw oti to
		//mhnyma einai broadcast
		//pernaw thn plhroforia sthn kefalh toy paketoy tmp	
		call RoutingAMPacket.setDestination(&tmp, AM_BROADCAST_ADDR);
		//pernaw to mege8os toy mhnymatos typoy RoutingMsg
		call RoutingPacket.setPayloadLength(&tmp, sizeof(RoutingMsg));
		//mexri edw exw ftia3ei to mhnyma
		
		dbg("SRTreeC", "In event RoutingMsgTimer.fired(...) : Created RoutingMsg : sender id = %d, sender depth = %d\n", mrpkt->senderID, mrpkt->depth);
		
		//kalw ena command poy bazei to mhnyma
		//sthn oyra pros apostolh
		enqueueDone=call RoutingSendQueue.enqueue(tmp);
		//den to stelnw twra.. 8a kanw post ena task gia meta
		
		//8a epistrepsei la8os an einai gemath h oyra
		if( enqueueDone==SUCCESS)
		{
			dbg("SRTreeC","In event RoutingMsgTimer.fired(...) : RoutingMsg enqueued successfully in SendingQueue!!!\n");
			
			//an h oyra exei mono ena mhnyma poy perimenei
			//an den einai ==1 tote den kanoyme tpt giati
			//kapoios allos exei kanei post ena task
			if (call RoutingSendQueue.size()==1)
			{
				//post toy task gia na trabh3ei to prwto pragma
				//apo thn oyra kai na to steilei
				post sendRoutingTask();
				//otan er8ei to shma oti stal8hke elegxei an 
				//exei kai alla na steilei kai 3anakanei
				//post ton eayto toy
				
				dbg("SRTreeC", "In event RoutingMsgTimer.fired(...) : sendRoutingTask() task posted!!\n");
			}
		}
		else
		{
			dbg("SRTreeC","In event RoutingMsgTimer.fired(...) : RoutingMsg failed to be enqueued in SendingQueue!!!");
		}		
	}
	
	//signal oti stal8hke to Routing mhnyma
	event void RoutingAMSend.sendDone(message_t * msg , error_t err)
	{
		dbg("SRTreeC", "In event RoutingAMSend.sendDone(...) : Routing package sent... %s \n",(err==SUCCESS)?"True":"False");
		
		//eley8eros asyrmatos
		setRoutingSendBusy(FALSE);
		
		//an den einai adeia h oyra twn mhnymatwn na steilw
		//kai allo Routing mhnyma 3anakanw post to task
		if(!(call RoutingSendQueue.empty()))
		{
			dbg("SRTreeC", "In event RoutingAMSend.sendDone(...) : RoutingSendQueue not empty, posting sendRoutingTask() task\n");
			post sendRoutingTask();
		}
	}
	
	event void NotifyAMSend.sendDone(message_t *msg , error_t err)
	{
		dbg("SRTreeC", "In event NotifyAMSend.sendDone(...) :  A Notify package sent... %s \n",(err==SUCCESS)?"True":"False");
	
		setNotifySendBusy(FALSE);
		
		if(!(call NotifySendQueue.empty()))
		{
			dbg("SRTreeC", "In event NotifyAMSend.sendDone(...) : NotifySendQueue not empty posting sendNotifyTask() task\n");
			post sendNotifyTask();
		}
	}

	//o pateras lambanei mia metrisi apo to paidi tou
	event message_t* NotifyReceive.receive( message_t* msg , void* payload , uint8_t len)
	{
		error_t enqueueDone;
		message_t tmp;
		uint16_t msource;
		
		msource = call NotifyAMPacket.source(msg);
		
		dbg("SRTreeC", "In event NotifyReceive.receive : ### NotifyReceive.receive() start ##### \n");
		dbg("SRTreeC", "In event NotifyReceive.receive : Something received!!!  from %u   %u \n",((NotifyParentMsg*) payload)->senderID, msource);

		atomic{
		memcpy(&tmp,msg,sizeof(message_t));
		//tmp=*(message_t*)msg;
		}
		enqueueDone=call NotifyReceiveQueue.enqueue(tmp);

		
		if( enqueueDone== SUCCESS)
		{
			dbg("SRTreeC", "In event NotifyReceive.receive : Msg enqueued in NotifyReceiveQueue successfully\n");
			post receiveNotifyTask();
			dbg("SRTreeC", "In event NotifyReceive.receive : receiveNotifyTask() tast posted\n");
		}
		else
		{
			dbg("SRTreeC","In event NotifyReceive.receive : NotifyMsg enqueue failed!!! \n");		
		}
		
		dbg("SRTreeC", "In event NotifyReceive.receive : ### NotifyReceive.receive() end ##### \n");
		return msg;
	}

	
	//edw exeis labei ena mhnyma kai 8a to baloyme sthn oyra
	//me ta mhnymata poy labame pros epe3ergasia
	//kai 8a kanoyme post ena task na to epe3ergastei argotera
	//msg deikths sto mhnyma, payload deikths sta dedomena
	//len mege8os twn dedomenwn
	event message_t* RoutingReceive.receive( message_t * msg , void * payload, uint8_t len)
	{
		error_t enqueueDone;
		message_t tmp;
		
		//agnooume ola ta broadcast munhmata pou stelnoun ta paidia tou kombou
		//arxika to depth arxikopoieitai sto 255 (einai uint ara to -1 = 255)
		if(((RoutingMsg*) payload)->depth < curdepth)
		{
			dbg("SRTreeC", "In event RoutingReceive.receive(...) : ### RoutingReceive.receive() start ##### \n");
			dbg("SRTreeC", "In event RoutingReceive.receive(...) : Msg received from %u\n",((RoutingMsg*) payload)->senderID);
			
			atomic{
			//antigrafei to mhnyma msg se mia metablhth tmp typoy
			//messgae_t    **XREIAZETAI des parakatw**
			memcpy(&tmp,msg,sizeof(message_t));
			//tmp=*(message_t*)msg;
			}
			//pros8etei to mhnyma sthn oyra mhnymatwn poy exw labei
			//typoy RoutinRecieve
			enqueueDone=call RoutingReceiveQueue.enqueue(tmp);
			//an den einai gemath h oyra epistrefei success
			if(enqueueDone == SUCCESS)
			{
				dbg("SRTreeC", "In event RoutingReceive.receive(...) : RoutingMsg enqueued successfully\n");
				//kanoyme post to task gia na epe3ergastei
				//to mhnyma poy elabe otan brei xrono
				post receiveRoutingTask();
				dbg("SRTreeC", "In event RoutingReceive.receive(...) : receiveRoutingTask() task posted!!\n");
			}
			//an einai gemath h oyra
			else
			{
				dbg("SRTreeC","In event RoutingReceive.receive(...) : RoutingMsg enqueue failed!!! \n");	
			}
			
			dbg("SRTreeC", "In event RoutingReceive.receive(...) : ### RoutingReceive.receive() end ##### \n");
		}
		else
		{
 			dbg("SRTreeC", "In event RoutingReceive.receive(...) : Received broadcast msg from my child\n");
 		}
			
		//PANTA otan kanw recieve epistrefw ena deikth
		//sto mhnyma poy ekana recieve
		//AN den eixa kanei prin copy to mhnyma se dikh moy
		//metablhth gia na to balw sthn oyra to epomeno
		//mhnyma poy 8a er8ei 8a graftei sthn idia 8esh mnhmhs
		return msg;
	}

	////////////// Tasks implementations //////////////////////////////
	
	
	task void sendRoutingTask()
	{
		//uint8_t skip;
		uint8_t mlen;
		uint16_t mdest;
		error_t sendDone;
		
		uint32_t t0;
		uint32_t random;
		
		

		//an h oyra mhnymatwn poy exw na steilw einai adeia
		//typwnei ena mhnyma kai epistrefei
		if (call RoutingSendQueue.empty())
		{
			dbg("SRTreeC","In task sendRoutingTask(...) : Error RoutingSendQueue is empty!\n");
			return;
		}
		
		//an einai true, dhladh an exeis 3kinhsei na steileis
		//ena allo Routing mhnyma kai den exeis parei pisw
		//signal oti stal8hke, tote perimeneis na teleiwsei
		//DEN 8a symbei (mono ama steilw polles fores to idio mnm)
		if(RoutingSendBusy)
		{
			dbg("SRTreeC","In task sendRoutingTask(...) : RoutingSendBusy= TRUE!!!\n");
			setLostRoutingSendTask(TRUE);
			return;
		}
		
		//dequeue: afairei apo thn oyra to prwto stoixeio
		//dhladh pairneis to mhnyma poy 8es na steileis
		radioRoutingSendPkt = call RoutingSendQueue.dequeue();
		
		
		//plhroforia oti to mhnyma exei mlen mhkos kai mdest
		//proorismo ka8ara gia debugging
		//8a mporoyse na mhn ginei
		mlen= call RoutingPacket.payloadLength(&radioRoutingSendPkt);
		mdest=call RoutingAMPacket.destination(&radioRoutingSendPkt);
		//elegxos an to mhnyma einai swstoy mege8oys
		//an to kanoyme swsta de 8a mpei pote edw
		if(mlen!=sizeof(RoutingMsg))
		{
			dbg("SRTreeC","In task sendRoutingTask(...) : Error Unknown message!!!\n");
			return;
		}
		//prospa8ei na steilei to mhnyma
		//otan kaloyme mia send to mhnyma mpainei se buffer..
		//kapoia sigmh petagetai ena shma sendDone.
		//ayto edw to sendDone einai mia metablhth poy dhlwnei
		//oti to mhnyma prow8h8hke ston buffer
		//exei to idio onoma gia na mperdeytoyme
		sendDone=call RoutingAMSend.send(mdest,&radioRoutingSendPkt,mlen);
		
		if ( sendDone== SUCCESS)
		{
			//typwnw mynhmata oti 3ekinhse na stelnetai
			dbg("SRTreeC","In task sendRoutingTask(...) : Msg forwarded successfully\n");
			//exw 3ekinhsei na stelnw kati kai to radiofwno
			//einai busy
			setRoutingSendBusy(TRUE);
			
			
			
		
			//arxikopoioume tin rand me (time(NULL) + TOS_NODE_ID) oste na kathe fora na exoume allo apotelesma 
			//to TOS_NODE_ID to bazoume oste oi komboi tou idiou epipedou na exoun allo apotelesma
			srand (time(NULL) + TOS_NODE_ID);
			//epilegoume ena tuxaio arithmo sto diastima [0, timePerDepth - 1]
			random = rand() % (timePerDepth);
			
			//treeInit -> xronos gia ti dimiourgia tou dendrou, idios gia olous tous kombous
			//TIMER_PERIOD_MILLI -> 60000ms mia epoxi
			//window -> exoume xorisei tin kathe epoxi se Max_depth * windows diastimata
			
			//oi komboi tou idiou epipedou theloume na steiloun mesa sto idio xroniko diastima pou tous exoume diathesei
			//to diastima auto einai to [(60000 - ( window * (curdepth +1))),   (60000 - ( window * (curdepth +1))) +  window    ]
			// gia na apofugoume tis sigkrouseis xrisimopoioume tin random gia na dialexoume mia tuxaia timh se auto to diastima 
			
			t0 = treeInit + (TIMER_PERIOD_MILLI - ( window * (curdepth +1) )) + random;
			
			dbg("SRTreeC","In task sendRoutingTask(...) : start at %d\n", t0);
			
			
			call ResponceTimer.startPeriodicAt(t0, TIMER_PERIOD_MILLI);
			
			
			
		}
		else
		{
			dbg("SRTreeC","In task sendRoutingTask(...) : Error Msg forwarding failed!!!\n");
			//setRoutingSendBusy(FALSE);
		}
	}
	/**
	 * dequeues a message and sends it
	 */
	task void sendNotifyTask()
	{
		uint8_t mlen;//, skip;
		error_t sendDone;
		uint16_t mdest;
		NotifyParentMsg* mpayload;
		
		if (call NotifySendQueue.empty())
		{
			dbg("SRTreeC","In task sendNotifyTask(...) : Error NotifySendQueue is empty!\n");
			return;
		}
		
		if(NotifySendBusy==TRUE)
		{
			dbg("SRTreeC","In task sendNotifyTask(...) : NotifySendBusy= TRUE!!!\n");
			setLostNotifySendTask(TRUE);
			return;
		}
		
		radioNotifySendPkt = call NotifySendQueue.dequeue();
		
		mlen=call NotifyPacket.payloadLength(&radioNotifySendPkt);
		
		mpayload= call NotifyPacket.getPayload(&radioNotifySendPkt,mlen);
		
		if(mlen!= sizeof(NotifyParentMsg))
		{
			dbg("SRTreeC", "In task sendNotifyTask(...) : Error Unknown message!!\n");
			return;
		}
		
		dbg("SRTreeC" , "In task sendNotifyTask(...) :  Sending NotifyParentMsg : mlen = %u  senderID= %u , count = %d, sum(x) = %d, sum(X^2) = %d\n",mlen,mpayload->senderID, mpayload->count, mpayload->sum, mpayload->sum2);

		mdest= call NotifyAMPacket.destination(&radioNotifySendPkt);
		
		sendDone=call NotifyAMSend.send(mdest,&radioNotifySendPkt, mlen);
		
		
		
		if ( sendDone== SUCCESS)
		{
			dbg("SRTreeC","In task sendNotifyTask(...) : Msg forwarded successfully\n");
			setNotifySendBusy(TRUE);
			
			// i riza tou dendrou embanizei to teliko apotelesma
			if ( TOS_NODE_ID==0)
			{
				
				
				
//				sum_tmp   = mpayload->sum  + TOS_NODE_ID;
//				sum2_tmp  = mpayload->sum2 + TOS_NODE_ID*TOS_NODE_ID;
//				count_tmp = mpayload->count + 1;

				dbg("SRTreeC", "############################################################################ \n");
				dbg("SRTreeC" , "In task sendNotifyTask(...) : sum(x) = %d, sum(X^2) = %d, count = %d\n", sum_tmp, sum2_tmp, count_tmp );
				dbg("SRTreeC" , "In task sendNotifyTask(...) : AVG = %f, Variance =%f\n", (float) sum_tmp/count_tmp, (float) sum2_tmp/count_tmp - pow(  (float) sum_tmp/count_tmp, 2));
				dbg("SRTreeC", "############################################################################ \n");
				
				if (roundCounter < Rounds)
				{
					dbg("SRTreeC", "##################################### \n");
					dbg("SRTreeC", "#######   ROUND   %u    ############## \n", roundCounter+1);
					dbg("SRTreeC", "#####################################\n");
				}
				
//				
//				dbg("SRTreeC" , "In task receiveNotifyTask(...) : %f\n", (float) sum2_tmp/count_tmp);
//				dbg("SRTreeC" , "In task receiveNotifyTask(...) : %f\n", (float) pow(  (float) sum_tmp/count_tmp, 2) );
//				dbg("SRTreeC" , "In task receiveNotifyTask(...) : %f\n", (float) sum2_tmp/count_tmp - pow(  (float) sum_tmp/count_tmp, 2) );

			}
			
			atomic
			{
				// kathe kombos sbinei tis metrisei molis tis steilei 
				// oste na steilei sostes times tin epomeni epoxi
				count_tmp = 0;
		        sum_tmp   = 0;
			    sum2_tmp  = 0; 
			}
		}
		else
		{
			dbg("SRTreeC","In task sendNotifyTask(...) : Error send failed!!!\n");
			//setNotifySendBusy(FALSE);
		}
	}
	////////////////////////////////////////////////////////////////////
	//*****************************************************************/
	///////////////////////////////////////////////////////////////////
	/**
	 * dequeues a message and processes it
	 */
	
	//otan labw mhnyma Routing prepei na to metadwsw kai egw
	//gia na to paroyn kai oi ypoloipoi panta Broadcast
	task void receiveRoutingTask()
	{
		uint8_t len;
		message_t radioRoutingRecPkt;
		
		RoutingMsg* mrpkt;
		message_t tmp2;
		error_t enqueueDone;
		
		
		//bgazw kati apo thn oyra kai blepw poios to esteile
		//ti plhroforia exei, ti ba8os htan aytos
		//afairw to prwto mhnyma apo thn oyra ws paketo
		radioRoutingRecPkt= call RoutingReceiveQueue.dequeue();
		
		//mege8os dedomenwn toy paketoy
		len= call RoutingPacket.payloadLength(&radioRoutingRecPkt);
		
		dbg("SRTreeC","In task receiveRoutingTask(...) : Received msg  len=%u \n",len);
	
		//panta 8a einai swsto an den kanoyme kapoio la8os
		if(len == sizeof(RoutingMsg))
		{
			//kalei getpayload apo to packet interface
			//kai epistrefei deikth sta dedomena toy mhnymatos
			//ta dedomena toy mhnymatos htan ena struct me onoma
			//RoutingMsg kai to kanw cast se deikth RoutingMsg
			RoutingMsg * mpkt = (RoutingMsg*) (call RoutingPacket.getPayload(&radioRoutingRecPkt,len));

			//twra mporw na prospelasw ta pedia pou htan mesa sto struct sto mhnyma poy elaba
			dbg("SRTreeC" , "In task receiveRoutingTask(...) : Msg is RoutingMsg : senderID= %d , depth= %d\n", mpkt->senderID , mpkt->depth);

			//elegxo an den exo patera
			if ( (parentID<0)||(parentID>=65535))
			{
				atomic
				{
					//tote den exei akoma patera
					//kai kanw patera ayton poy moy esteile to mhnyma
					parentID= call RoutingAMPacket.source(&radioRoutingRecPkt);//mpkt->senderID;q
					//to ba8os moy einai to ba8os toy patera+1
					curdepth= mpkt->depth + 1;

					count_tmp = 0;
					sum_tmp = 0;
					sum2_tmp = 0;
				}
				
				dbg("SRTreeC","In task receiveRoutingTask(...) : Node initialisation : parentID = %d, depth = %d\n", parentID, curdepth);
				
				//kataskeui tou RoutingMsg minimatos gia na ginei broadcast sta paidia tou
				mrpkt = (RoutingMsg*) (call RoutingPacket.getPayload(&tmp2, sizeof(RoutingMsg)));
				
				if(mrpkt==NULL)
				{
					dbg("SRTreeC","In task receiveRoutingTask(...) : Error: No valid payload... \n");
					return;
				}
				
				atomic
				{
					mrpkt->senderID=TOS_NODE_ID;
					mrpkt->depth = curdepth;
				}		
				
				call RoutingAMPacket.setDestination(&tmp2, AM_BROADCAST_ADDR);
				call RoutingPacket.setPayloadLength(&tmp2, sizeof(RoutingMsg));
				
				dbg("SRTreeC" , "In task receiveRoutingTask(...) : Created RoutingMsg : sender id = %d, sender depth = %d\n", mrpkt->senderID, mrpkt->depth);
				
				enqueueDone=call RoutingSendQueue.enqueue(tmp2);
								
				if( enqueueDone==SUCCESS)
				{
					dbg("SRTreeC", "In task receiveRoutingTask(...) : RoutingMsg enqueued successfully\n");
					if (call RoutingSendQueue.size()==1)
					{
						post sendRoutingTask();
						dbg("SRTreeC", "In task receiveRoutingTask(...) : sendRoutingTask() task posted!!\n");
					}
				}
				else
				{
					dbg("SRTreeC","In task receiveRoutingTask(...) : RoutingMsg failed to be enqueued in SendingQueue!!!");
				}
				
				
				//an den eimai o kombos 0
				if (TOS_NODE_ID!=0)
				{	
					//xekinao ena metriti pou otan xtupisei tha steilo tin metrisi ston patera mou
					//call ResponceTimer.startOneShot(mpkt->responce  + (rand() % 10 + 1)*20);
				    //dbg("SRTreeC" , "In task receiveRoutingTask(...) :  ResponceTimer started : %d\n", mpkt->responce + rand()/10000);
				    
				    

				    
				}
			}
			//an lavo RoutingMsg eno exo eidi epilexei patera to agnoo
			else
			{
				
				//an to depth poy eixa einai megalytero apo
				//ayto poy elaba+1 tote o kainoyrios einai
				//kalyteros pateras
				if (( curdepth > mpkt->depth +1) || (mpkt->senderID==parentID))
				{
					dbg("SRTreeC", "In task receiveRoutingTask(...) : Choosing new father \n");
//					//allagh patera
//					uint16_t oldparentID = parentID;
//					
//					//3anastelnw Routing mhnyma
//					parentID= call RoutingAMPacket.source(&radioRoutingRecPkt);//mpkt->senderID;
//					curdepth = mpkt->depth + 1;
				}
			}
		}
		else
		{
			dbg("SRTreeC","receiveRoutingTask():Empty message!!! \n");
			setLostRoutingRecTask(TRUE);
			return;
		}
	}

	
	 
	task void receiveNotifyTask()
	{
		uint8_t len;
		message_t radioNotifyRecPkt;
		radioNotifyRecPkt= call NotifyReceiveQueue.dequeue();
		
		len= call NotifyPacket.payloadLength(&radioNotifyRecPkt);
		
		
	
	
		
		dbg("SRTreeC","In task receiveNotifyTask(...) : Received Msg len=%u \n",len);

		if(len == sizeof(NotifyParentMsg))
		{
			// an to parentID== TOS_NODE_ID tote
			// tha proothei to minima pros tin riza xoris broadcast
			// kai tha ananeonei ton tyxon pinaka paidion..
			// allios tha diagrafei to paidi apo ton pinaka paidion
			
			NotifyParentMsg* mr = (NotifyParentMsg*) (call NotifyPacket.getPayload(&radioNotifyRecPkt,len));
			
			dbg("SRTreeC" , "In task receiveNotifyTask(...) : Msg is NotifyParentMsg from %d !!! \n", mr->senderID);

			if ( mr->parentID == TOS_NODE_ID)
			{
				// tote prosthiki stin lista ton paidion.
				sum_tmp   = sum_tmp + mr->sum;
				sum2_tmp  = sum2_tmp + mr->sum2;
				count_tmp = count_tmp + mr->count;
				
				dbg("SRTreeC" , "In task receiveNotifyTask(...) : Aggregated sum(x) = %d, Aggregated Sum(x^2) = %d, Aggregated Count = %d\n", sum_tmp, sum2_tmp, count_tmp);
			}
			else
			{
				// h metrisi pou elabe den einai apo paidi tou
				
			}
		}
		else
		{
			dbg("SRTreeC","receiveNotifyTask():Empty message!!! \n");
			setLostNotifyRecTask(TRUE);
			return;
		}
		
	}
	// otan kanei fired autos o timer to paidi xekinaei na apantisei ston patera tou
		event void ResponceTimer.fired()
	{
		NotifyParentMsg* m;
		message_t tmp;
		nx_uint32_t random;
		
		dbg("SRTreeC","In event ResponceTimer.fired() : \n");
		
		if (roundCounter == Rounds-1){
			call ResponceTimer.stop();
			dbg("SRTreeC","In event ResponceTimer.fired() : stop timer\n");
		}
		roundCounter+=1;
		
		//kataskeuazoume to minima me tis metriseis pou tha steiloume ston patera
		m = (NotifyParentMsg *) (call NotifyPacket.getPayload(&tmp, sizeof(NotifyParentMsg)));
		
		atomic
		{
			m->senderID=TOS_NODE_ID;
			m->depth = curdepth;
			m->parentID = parentID;
			
			
			//sum_tmp    = sum_tmp  + TOS_NODE_ID;
			//sum2_tmp   = sum2_tmp + TOS_NODE_ID*TOS_NODE_ID;

			//San metrisi tou kombou epilegoume tuxaia enan akeraio sto diastima [0,20]
			//kai sti sunexeia tou prosthetoume to ID tou kombou
			srand (time(NULL));
			random = rand() % 21;  //h rand()%20 bgazei random sto [0-19]
			sum_tmp    = sum_tmp  + TOS_NODE_ID + random;
			sum2_tmp   = sum2_tmp + (TOS_NODE_ID + random)* (TOS_NODE_ID + random);
			
			count_tmp += 1;
			
			
			m->count = count_tmp;
			m->sum   = sum_tmp;
			m->sum2  = sum2_tmp;
		}
		
		dbg("SRTreeC" , "In event ResponceTimer.fired()  : senderId = %d, sender depth = %d, parentId = %d, Metrish =  %d\n", m->senderID, m->depth, m->parentID, TOS_NODE_ID + random);
		
		call NotifyAMPacket.setDestination(&tmp, parentID);
		call NotifyPacket.setPayloadLength(&tmp,sizeof(NotifyParentMsg));
		
		if (call NotifySendQueue.enqueue(tmp)==SUCCESS)
		{
			dbg("SRTreeC", "In event ResponceTimer.fired() : NotifyParentMsg enqueued in SendingQueue successfully!!!\n");
			if (call NotifySendQueue.size() == 1)
			{
				post sendNotifyTask();
				dbg("SRTreeC", "In event ResponceTimer.fired() : sendNotifyTask() task posted)\n");
			}
		}
	}
	
}
