#include "SimpleRoutingTree.h"

configuration SRTreeAppC @safe() { }
implementation{
	components SRTreeC;	//kyrio module - programma


	//MainC: parexei to boot interface
	//ActiveMessageC: anoigma-kleisimo asyrmatoy
	//SerialActiveMessageC: dedomena sto pc mesw usb 8yras **den xreiazetai**
	components MainC, ActiveMessageC;
	components new TimerMilliC() as RoutingMsgTimerC;
	components new TimerMilliC() as ResponceTimerC;         //<-------------------
	
	components new AMSenderC(AM_ROUTINGMSG) as RoutingSenderC;
	components new AMReceiverC(AM_ROUTINGMSG) as RoutingReceiverC;
	components new AMSenderC(AM_NOTIFYPARENTMSG) as NotifySenderC;
	components new AMReceiverC(AM_NOTIFYPARENTMSG) as NotifyReceiverC;
	//oures mhnymatwn - ena zeygari gia ka8e neo typo mhnymatwn
	components new PacketQueueC(SENDER_QUEUE_SIZE) as RoutingSendQueueC;		//stelnw
	components new PacketQueueC(RECEIVER_QUEUE_SIZE) as RoutingReceiveQueueC;	//lambanw
	components new PacketQueueC(SENDER_QUEUE_SIZE) as NotifySendQueueC;
	components new PacketQueueC(RECEIVER_QUEUE_SIZE) as NotifyReceiveQueueC;
	
	//To wiring einai swsto!
	SRTreeC.Boot->MainC.Boot;
	
	SRTreeC.RadioControl -> ActiveMessageC;	//radiofwno
	
	SRTreeC.RoutingMsgTimer->RoutingMsgTimerC;
	SRTreeC.ResponceTimer->ResponceTimerC;       //<---------------------------
	
	//Interface Packet:dinei synarthsh gia na paroyme deikth sta
	//dedomena toy paketoy gia to payload
	SRTreeC.RoutingPacket->RoutingSenderC.Packet;
	//Interface AMPacket:dinei synarthsh thn source gia na broyme
	//poios einai o apostoleas toy mhnymatos
	SRTreeC.RoutingAMPacket->RoutingSenderC.AMPacket;
	//stelnoyme mhnymata
	SRTreeC.RoutingAMSend->RoutingSenderC.AMSend;
	//lambanoyme mhnymata
	SRTreeC.RoutingReceive->RoutingReceiverC.Receive;
	
	//omoiws
	SRTreeC.NotifyPacket->NotifySenderC.Packet;
	SRTreeC.NotifyAMPacket->NotifySenderC.AMPacket;
	SRTreeC.NotifyAMSend->NotifySenderC.AMSend;
	SRTreeC.NotifyReceive->NotifyReceiverC.Receive;
	
	//oures mhnymatwn - ena zeygari gia ka8e neo typo mhnymatwn
	SRTreeC.RoutingSendQueue->RoutingSendQueueC;
	SRTreeC.RoutingReceiveQueue->RoutingReceiveQueueC;
	SRTreeC.NotifySendQueue->NotifySendQueueC;
	SRTreeC.NotifyReceiveQueue->NotifyReceiveQueueC;
	
}
